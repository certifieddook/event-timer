use axum::http::StatusCode;
use axum::response::IntoResponse;

use tracing::log::error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("resource could not be found")]
    NotFound,

    #[error("an internal server error occurred")]
    Eyre(#[from] eyre::Error),
}

impl Error {
    fn status_code(&self) -> StatusCode {
        match self {
            Error::NotFound => StatusCode::NOT_FOUND,
            Error::Eyre(_) => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}

impl IntoResponse for Error {
    fn into_response(self) -> axum::response::Response {
        match self {
            Error::Eyre(ref err) => error!("Internal error: {:?}", err),
            _ => (),
        }

        (self.status_code(), self.to_string()).into_response()
    }
}
