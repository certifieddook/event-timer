use axum::{routing::get, Router};

use crate::state::AppState;

use self::http::{create_timer, delete_timer, get_timer, list_timers, update_timer};

mod http;
pub mod models;

pub fn router() -> Router<AppState> {
    Router::new()
        .route("/api/timers", get(list_timers).post(create_timer))
        .route(
            "/api/timers/:timer_id",
            get(get_timer).put(update_timer).delete(delete_timer),
        )
}
