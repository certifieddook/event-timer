use eyre::Context;
use futures::Stream;
use std::{collections::BTreeMap, convert::Infallible};
use tracing_unwrap::ResultExt;

use crate::{
    error::{Error, Result},
    state_manager::Message,
    timer::models::Timer,
};
use axum::{
    extract::{Path, State},
    http::StatusCode,
    response::{
        sse::{Event, KeepAlive},
        Sse,
    },
    Json,
};
use tokio::sync::{mpsc, watch};
use tokio_stream::wrappers::WatchStream;
use tokio_stream::StreamExt as _;

pub async fn create_timer(
    State(message_tx): State<mpsc::Sender<Message>>,
    State(timer_rx): State<watch::Receiver<BTreeMap<i32, Timer>>>,
    Json(params): Json<Timer>,
) -> Result<(StatusCode, Json<Timer>)> {
    message_tx
        .send(Message::Create(params.clone()))
        .await
        .wrap_err("could not send message")?;

    Ok((
        StatusCode::CREATED,
        Json(
            timer_rx
                .borrow()
                .last_key_value()
                .ok_or(Error::NotFound)?
                .1
                .clone(),
        ),
    ))
}

pub async fn get_timer(
    State(timer_rx): State<watch::Receiver<BTreeMap<i32, Timer>>>,
    Path(timer_id): Path<i32>,
) -> Result<Json<Timer>> {
    let borrow = timer_rx.borrow();
    let event = borrow.get(&timer_id).ok_or(Error::NotFound)?;

    Ok(Json(event.clone()))
}

pub async fn delete_timer(
    Path(timer_id): Path<i32>,
    State(message_tx): State<mpsc::Sender<Message>>,
) -> Result<StatusCode> {
    message_tx
        .send(Message::Delete(timer_id))
        .await
        .wrap_err("could not send message")?;

    Ok(StatusCode::NO_CONTENT)
}

pub async fn update_timer(
    State(message_tx): State<mpsc::Sender<Message>>,
    State(timer_rx): State<watch::Receiver<BTreeMap<i32, Timer>>>,
    Path(timer_id): Path<i32>,
    Json(params): Json<Timer>,
) -> Result<Json<Timer>> {
    message_tx
        .send(Message::Update(params.clone()))
        .await
        .wrap_err("could not send message")?;

    Ok(Json(
        timer_rx
            .borrow()
            .get(&timer_id)
            .ok_or(Error::NotFound)?
            .clone(),
    ))
}

pub async fn list_timers(
    State(events_rx): State<watch::Receiver<BTreeMap<i32, Timer>>>,
) -> Sse<impl Stream<Item = Result<Event, Infallible>>> {
    let stream = WatchStream::new(events_rx)
        .map(|payload| {
            Event::default()
                .json_data(Vec::from_iter(payload.values()))
                .expect_or_log("could not serialize data")
        })
        .map(Ok);

    Sse::new(stream).keep_alive(KeepAlive::default())
}
