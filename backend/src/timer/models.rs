use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use serde_trim::string_trim;
use typeshare::typeshare;

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[typeshare]
pub struct Timer {
    pub id: i32,
    #[serde(deserialize_with = "string_trim")]
    pub location: String,
    #[serde(deserialize_with = "string_trim")]
    pub name: String,
    #[typeshare(serialized_as = "Date")]
    pub begins: DateTime<Utc>,
    #[typeshare(serialized_as = "Date")]
    pub ends: DateTime<Utc>,
    #[typeshare(serialized_as = "Date")]
    pub yellow_margin: DateTime<Utc>,
    #[typeshare(serialized_as = "Date")]
    pub red_margin: DateTime<Utc>,
}
