use std::collections::BTreeMap;

use axum_macros::FromRef;
use tokio::sync::{
    mpsc,
    watch::{self},
};

use crate::{state_manager::Message, timer::models::Timer};

#[derive(Clone, FromRef)]
pub struct AppState {
    pub message_tx: mpsc::Sender<Message>,
    pub timers_rx: watch::Receiver<BTreeMap<i32, Timer>>,
}
