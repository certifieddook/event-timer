use std::{net::SocketAddr, path::PathBuf};

use clap::Parser;

#[derive(Parser, Clone, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    /// The socket address to bind to
    #[arg(short, long, env = "BIND", default_value = "0.0.0.0:8080")]
    pub bind: SocketAddr,

    /// The path to the timers CSV file
    #[arg(short, long, env = "CSV_PATH", default_value = "./timers.csv", value_parser = path_exists)]
    pub csv_path: PathBuf,

    /// The admin password
    #[arg(short, long, env = "password")]
    pub password: String,

    /// The allowed origin for CORS
    #[arg(short, long, env = "ALLOWED_ORIGIN", default_value = "localhost:8080")]
    pub allowed_origin: String,
}

fn path_exists(s: &str) -> Result<PathBuf, String> {
    let path: PathBuf = s.into();

    if !path.exists() {
        Err("CSV file does not exist".to_owned())
    } else if !path.is_file() {
        Err("CSV path must point to a file".to_owned())
    } else {
        Ok(path)
    }
}
