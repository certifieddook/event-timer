use std::{
    collections::BTreeMap,
    path::{Path, PathBuf},
};

use crate::error::Result;

use eyre::Context;
use serde::Serialize;
use tokio::{
    sync::{mpsc, watch},
    task::spawn_blocking,
};
use tracing_unwrap::ResultExt;

use crate::timer::models::Timer;

#[derive(Serialize, Debug, Clone)]
pub enum Message {
    Create(Timer),
    Update(Timer),
    Delete(i32),
}

pub struct StateManager {
    message_rx: mpsc::Receiver<Message>,
    timers_tx: watch::Sender<BTreeMap<i32, Timer>>,
    state: BTreeMap<i32, Timer>,
    csv_path: PathBuf,
}

impl StateManager {
    pub fn new(
        message_rx: mpsc::Receiver<Message>,
        timers_tx: watch::Sender<BTreeMap<i32, Timer>>,
        state: BTreeMap<i32, Timer>,
        csv_path: PathBuf,
    ) -> Self {
        Self {
            message_rx,
            timers_tx,
            state,
            csv_path,
        }
    }

    fn handle_event_change(&mut self, message: Message) {
        tracing::info!("{:?}", message);

        match message.clone() {
            Message::Create(params) => {
                let new_id = if let Some((last_id, _)) = self.state.last_key_value() {
                    last_id + 1
                } else {
                    1
                };
                let event = Timer {
                    id: new_id,
                    ..params
                };
                self.state.insert(new_id, event);
            }
            Message::Update(params) => {
                self.state.get(&params.id).get_or_insert(&params);
            }
            Message::Delete(id) => {
                self.state.remove(&id);
            }
        };

        // Update the csv file
        let state = self.state.clone();
        let csv_path = self.csv_path.clone();
        spawn_blocking(move || save_state(state, &csv_path));

        self.timers_tx
            .send(self.state.clone())
            .expect_or_log("state to be sent")
    }

    pub async fn run(mut self) {
        while let Some(message) = self.message_rx.recv().await {
            self.handle_event_change(message)
        }
    }
}

pub fn create_state(csv_path: &Path) -> Result<BTreeMap<i32, Timer>> {
    let mut rdr = csv::Reader::from_path(csv_path).wrap_err("could not create csv reader")?;
    let mut state = BTreeMap::<i32, Timer>::new();
    for result in rdr.deserialize() {
        let timer: Timer = result.wrap_err("could not deserialize csv record")?;
        state.insert(timer.id, timer);
    }
    Ok(state)
}

pub fn save_state(state: BTreeMap<i32, Timer>, csv_path: &Path) -> Result<()> {
    let mut wtr = csv::Writer::from_path(&csv_path).wrap_err("could not create csv writer")?;
    for (_, timer) in state {
        wtr.serialize(timer)
            .wrap_err("could not serialize timer to csv")?;
    }
    wtr.flush().wrap_err("could not flush csv")?;
    Ok(())
}
