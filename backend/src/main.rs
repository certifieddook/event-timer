use std::collections::BTreeMap;

use axum::Router;
use clap::Parser;
use eyre::Context;

use state_manager::{create_state, Message, StateManager};
use timer::models::Timer;
use tokio::sync::{mpsc, watch};
use tower_http::trace::TraceLayer;
use tracing::info;

use crate::{cli::Args, security::create_cors_layer, state::AppState};

mod assets;
mod cli;
mod error;
mod security;
mod state;
mod state_manager;
mod timer;

#[tokio::main]
async fn main() -> eyre::Result<()> {
    if std::env::var("NO_COLOR") == Err(std::env::VarError::NotPresent) {
        color_eyre::install()?;
        tracing_subscriber::fmt::init();
    } else {
        tracing_subscriber::fmt().with_ansi(false).init();
    }

    let args = Args::parse();

    // Create the initial empty state
    let state = create_state(&args.csv_path)?;

    // Prepare the channels
    let (message_tx, message_rx) = mpsc::channel::<Message>(1);
    let (timers_tx, timers_rx) = watch::channel::<BTreeMap<i32, Timer>>(state.clone());

    // Start the state manager
    let state_manager = StateManager::new(message_rx, timers_tx, state, args.csv_path.clone());
    tokio::spawn(state_manager.run());

    // Prepare the AppState
    let app_state = AppState {
        message_tx,
        timers_rx,
    };

    // Start axum
    serve(&args, app_state).await?;

    Ok(())
}

#[allow(clippy::missing_errors_doc)]
pub async fn serve(args: &Args, app_state: AppState) -> eyre::Result<()> {
    let app = Router::new()
        .merge(api_router())
        .layer(create_cors_layer(args))
        .layer(TraceLayer::new_for_http())
        .with_state(app_state);

    info!("Server started at http://{}", args.bind);

    let listener = tokio::net::TcpListener::bind(&args.bind).await?;
    axum::serve(listener, app.into_make_service())
        .await
        .wrap_err("error running HTTP server")
}

fn api_router() -> Router<AppState> {
    assets::router().merge(timer::router())
}
