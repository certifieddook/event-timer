### Build the frontend
FROM docker.io/library/node:18-slim AS frontend
# Install quasar/cli on the container
RUN yarn global add @quasar/cli

# Copy the base project files and perform an installation to cache it separately
COPY ./frontend/package.json ./frontend/package.json
COPY ./frontend/yarn.lock ./frontend/yarn.lock
RUN cd ./frontend && yarn install

# Copy the rest of the important project files
COPY ./frontend/tsconfig.json ./frontend/tsconfig.json
COPY ./frontend/postcss.config.js ./frontend/postcss.config.js
COPY ./frontend/.eslintrc.js ./frontend/.eslintrc.js
COPY ./frontend/public ./frontend/public
COPY ./frontend/index.html ./frontend/index.html
COPY ./frontend/quasar.config.js ./frontend/quasar.config.js
COPY ./frontend/src ./frontend/src

# Build the frontend
RUN cd ./frontend && quasar build

### Build the backend
FROM docker.io/library/rust:slim AS backend
ARG DEBIAN_FRONTEND=noninteractive

# Create a blank project to cache the dependencies
RUN cargo new backend

# Copy the backend files
COPY ./backend/Cargo.toml ./backend/Cargo.toml

# Download and cache the dependencies only
RUN cd ./backend && cargo build --release

# Copy the fronend and build the solution
COPY --from=frontend ./frontend/dist ./frontend/dist

# Copy the backend source files
COPY ./backend/migrations ./backend/migrations
COPY ./backend/src ./backend/src

# Build the backend
RUN cd ./backend && cargo clean && cargo build --release

FROM docker.io/library/debian:stable-slim
ARG DEBIAN_FRONTEND=noninteractive
COPY --from=backend ./backend/target/release/event-timer ./
CMD ["./event-timer"]
